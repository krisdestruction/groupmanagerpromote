package info.terrismc.GroupManagerPromote;

import java.util.Map;
import java.util.Set;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

public class ConfigStore {
	private GroupManagerPromote plugin;
	private FileConfiguration config;
	
	public ConfigStore( GroupManagerPromote plugin ) {
		this.plugin = plugin;
		reloadConfig();
	}
	
	public void reloadConfig() {
		plugin.saveDefaultConfig();
		plugin.reloadConfig();
		config = plugin.getConfig();
	}

	public Map<String, Object> getPromoteLadderMap( String ladderName ) {
		return getLadderMap( ladderName, "promoteLadders" );
	}

	public Map<String, Object> getDemoteLadderMap( String ladderName ) {
		return getLadderMap( ladderName, "demoteLadders" );
	}
	
	private Map<String, Object> getLadderMap( String ladderName, String ladderSectionName ) {
		// null ladderName means default ladder
		
		// Get ladder names set
		ConfigurationSection ladderListSection = config.getConfigurationSection( ladderSectionName );
		if( ladderListSection == null ) {
			plugin.getLogger().warning("Cannot get " + ladderSectionName + " section. Check config for this node name.");
			return null;
		}
		Set<String> ladderSet = ladderListSection.getKeys( false );
		
		// Check ladder set size to find default
		if( ladderName == null || ladderSet.size() == 1 ) {
			ladderName = "default";
		}
		else if( !ladderSet.contains( ladderName ) ) {
			return null;
		}
		
		// Ladder name exists, return rank list
		ConfigurationSection ladderSection = ladderListSection.getConfigurationSection( ladderName );
		if( ladderSection == null )
			return null;
		return ladderSection.getValues( false );
	}
	
}