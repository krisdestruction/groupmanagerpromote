package info.terrismc.GroupManagerPromote;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class GroupManagerPromote extends JavaPlugin {
    public static Permission permission = null;
	public ConfigStore cStore;
	
	public void onEnable() {
		// Setup vault
		setupPermissions();
		
		// Create storage objects
		cStore = new ConfigStore( this );
		CommandListener cl = new CommandListener( this );
		getCommand( "gmp" ).setExecutor( cl );
		getCommand( "manupromote" ).setExecutor( cl );
		getCommand( "manudemote" ).setExecutor( cl );
	}

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration( net.milkbowl.vault.permission.Permission.class );
        if ( permissionProvider != null )
            permission = permissionProvider.getProvider();
        
        return ( permission != null );
    }
}
