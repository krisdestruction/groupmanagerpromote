package info.terrismc.GroupManagerPromote;

import java.util.Map;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandListener implements CommandExecutor {
	private GroupManagerPromote plugin;
	private ConfigStore cStore;
	
	public CommandListener( GroupManagerPromote plugin ) {
		this.plugin = plugin;
		this.cStore = this.plugin.cStore;
	}
	
	private boolean hasPermission( CommandSender sender, String permission ) {
		if( sender instanceof Player ) {
			Player player = (Player) sender;
			if( !player.hasPermission( permission ) ) {
				player.sendMessage( "You do not have permission for this command" );
				return false;
			}
		}
		
		return true;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		// Print otherwise return command help if no parameters
		if( cmd.getName().equals( "gmp" ) ) {
			// Check permission
			if( !hasPermission( sender, "gmp.admin" ) ) return true;
			
			// Reload config
			cStore.reloadConfig();
			sender.sendMessage( "Plugin config reloaded." );
			return true;
		}
		else if ( args.length == 0 ){
			return false;
		}		
		
		// Check permission
		if( !hasPermission( sender, "gmp.promote" ) ) return true;
		
		// Get player
		String playerName = args[0];
		
		// Check player name
		if( plugin.getServer().getPlayer( playerName ) == null && !plugin.getServer().getOfflinePlayer( playerName ).hasPlayedBefore() ) {
			// No player found
			sender.sendMessage("Error: The specified player has never joined the server before. Specify a player who has joined the server before.");
			return true;
		}
		
		// Declare group and world parameters
		String ladderGroup;
		World world;
		
		if( args.length == 1 ) {
			// Promote user on default ladder
			ladderGroup = null;
			world = getSenderWorld( sender );
		}
		else if( args.length == 2 ) {
			// Promote user on ladder
			ladderGroup = args[1];
			world = getSenderWorld( sender );
		}
		else {
			// Promote user on ladder with world
			ladderGroup = args[1];
			String worldName = args[2];
			world = plugin.getServer().getWorld( worldName );
		}
		
		// Check world
		if( world == null ) {
			sender.sendMessage( "Error: The specified world does not exist. Specify a valid world." );
			return true;
		}
		
		// Get group ladder mapping
		String cmdName = cmd.getName();
		Map<String, Object> ladderGroupMap;
		
		// Promote over ladder group
		// TODO: Check permission for ladder group
		if( cmdName.equals( "manupromote" ) )
			ladderGroupMap = cStore.getPromoteLadderMap( ladderGroup );
		else   // manudemote case, gmp is stopped earlier
			ladderGroupMap = cStore.getDemoteLadderMap( ladderGroup );
		
		// Ladder error check
		if( ladderGroupMap == null ) {
			if( ladderGroup == null )
				sender.sendMessage( "Error: A default ladder does not exist. Specify a default ladder in the config file." );
			else
				sender.sendMessage( "Error: The specified ladder " + ladderGroup + " does not exist. See if this ladder is in the config file." );
			return true;
		}
		
		return changePlayerGroup( sender, ladderGroupMap, playerName, world );
	}
	
	private boolean changePlayerGroup( CommandSender sender, Map<String, Object> ladderGroupMap, String playerName, World world ) {
		// Promote player on ladder group list
		
		// Player group array to set
		String playerPrimaryGroup = GroupManagerPromote.permission.getPrimaryGroup( world, playerName );
		String playerNextGroup = (String) ladderGroupMap.get( playerPrimaryGroup );
		
		if( playerNextGroup == null ) {
			// No promotion group
			sender.sendMessage( "No rank to switched player into. Check ladder config definitions for the rank " + playerPrimaryGroup + "." );
		}
		else {
			// Promotion group found
			
			// Remove current group
			if ( !GroupManagerPromote.permission.playerRemoveGroup( world, playerName, playerPrimaryGroup ) ) {
				sender.sendMessage( "Error: Failed to remove player from group. Report issue to developer." );
				return true;
			}
				
			// Add promoted group
			if ( !GroupManagerPromote.permission.playerAddGroup( world, playerName, playerNextGroup ) ) {
				sender.sendMessage( "Error: Failed to add player to group. Report issue to developer." );
			}
			else {
				sender.sendMessage( playerName + " rank was switched to " + playerNextGroup );
				Player targetPlayer = plugin.getServer().getPlayer( playerName );
				if( targetPlayer.isOnline() )
					targetPlayer.sendMessage( "Your rank was switched to " + playerNextGroup );
			}
		}
		
		return true;
	}
	
	private World getSenderWorld( CommandSender sender ) {
		// Check if player
		if( !(sender instanceof Player ) ) {
			// TODO: Detect player world if online
			sender.sendMessage( "Error: You are not a player. Specify the rank to use this command." );
			return null;
		}
		
		// Get world implicitly
		Player playerSender = (Player) sender;
		return playerSender.getWorld();
	}
}